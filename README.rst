==================================
Git repository for OpenREM website
==================================

This is the repository for the OpenREM website, which is to be found at
http://openrem.org.

If you want to help improve the website, that's great! Please feel free
to fork this repository, make your improvements in your version then
send me a pull request :-)

You can also log issues here, if you are logged in (and you can use all
sorts of logins).

I look forward to hearing from you!
